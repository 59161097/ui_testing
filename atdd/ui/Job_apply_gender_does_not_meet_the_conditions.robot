***Settings***
# Library
Library   SeleniumLibrary

***Variables***


***Test Cases***
บุริศร์ ค้นหางาน Programmer และ ดูเพศไม่ตรงตามเงื่อนไข
    #0: ต้องทำอะไรบ้าง?
    เข้าเว็บไปที่หน้าค้นหา
    พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    เลือกตำแหน่งงาน Junior Node Programmer
    กดสมัคร
    ระบบแสดงผลลัพท์ว่า Gender does not meet the conditions

**Keywords**
เข้าเว็บไปที่หน้าค้นหา
    Open Browser   http://localhost:3000/searchJob    chrome
    Set Selenium Speed    0.7

พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา    
    Input Text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    Element Text Should Be    id=name_4    Junior Node Programmer
    
เลือกตำแหน่งงาน Junior Node Programmer
    Click Element    id=show_detail_4
    Wait Until Element Contains     id=name    Junior Node Programmer

กดสมัคร
    Click Element    id=apply_job

ระบบแสดงผลลัพท์ว่า เพศไม่ตรงกับเงื่อนไข
    Element Should Contain    id=message    Gender does not meet the conditions
    